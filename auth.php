<?php
date_default_timezone_set('Australia/Sydney');

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); 
}

require_once($CFG->libdir.'/authlib.php');
require_once('lib/nusoap.php');

class auth_plugin_ipaa extends auth_plugin_base {
	
	function auth_plugin_ipaa() {
        $this->authtype = 'ipaa';
        $this->config = get_config('auth/ipaa');
        if (empty($this->config->extencoding)) {
            $this->config->extencoding = 'utf-8';
        }
    }
	
	function user_login($username, $password) {
		global $DB;
		$nusoap_response = $this->nusoap_config($username, $password);
		if($nusoap_response['userlogindetails']['loginResult']['errorDescription']=="Success"){
			$exist = $DB->get_record('user', array('username'=>$username));
			if($exist){
				$this->update_user($username);
			}
			return true;
		}else{
			return false;
		}
	}
	
	public function nusoap_config($username, $password){
		$GLOBALS['passwordtemp'] = $password;
    	$wsdl = 'https://www.ipaansw.ipaa.org.au/IPAA/IPAANSWServices.asmx?WSDL';
    	$client = new nusoap_client($wsdl, 'WSDL');
    	$nusoapresult = array();
    	$nusoapresult['userlogindetails'] = $client->call('login', array('username' => strtolower($username), 'password' => $GLOBALS['passwordtemp']));
    	$nusoapresult['userinfo'] = $client->call('getDemo', array('id'=>$nusoapresult['userlogindetails']['id'], 'demoRecord'=>array()));
    	$nusoapresult['marketcode'] = $client->call('getMarkets', array('id'=>$nusoapresult['userlogindetails']['id'], 'code'=>'GENDER', 'markets'=>array()));
    	$nusoapresult['gender'] = $nusoapresult['marketcode']['markets']['Market'][0]['marketSub_code'];
    	$query = "SELECT reg_code FROM conf WHERE id = ".$nusoapresult['userlogindetails']['id']." AND code = 'INDMEM' AND (pay_code IN ('P','F','G','H')) AND memb_end > DATE()";
    	$nusoapresult['membercode'] = $client->call('QuerySearch', array('sqlstring'=>$query));
    	$nusoapresult['memberstatus'] = json_encode(simplexml_load_string($nusoapresult['membercode']['QuerySearchResult']));
    	$nusoapresult['memcode'] = json_decode($nusoapresult['memberstatus'],true);
    	
    	return $nusoapresult;
    }
	
	function get_userinfo($username) {
		$nusoapresult = $this->nusoap_config($username, $GLOBALS['passwordtemp']);
		$userinfo = array();

			if(isset($nusoapresult['userinfo'])){
			foreach($nusoapresult['userinfo'] as $result){
				$userinfo['idnumber'] = trim($result['demoRef_id']," ");
				$userinfo['firstname'] = strtolower(trim($result['demoGiv']," "));
				$userinfo['lastname'] = strtolower(trim($result['demoSur']," "));
				$userinfo['email'] = strtolower(trim($result['demoE_mail']," "));
				$userinfo['country'] = trim($result['demoCountry']," ");
				//custom profile fields
				$userinfo['profile_field_state'] = strtolower(trim($result['demoState']," "));
				$userinfo['profile_field_dob'] = trim($result['demoDob']," ");
				$userinfo['profile_field_position'] = strtolower(trim($result['demoPosit']," "));
				$userinfo['profile_field_organisation'] = strtolower(trim($result['demoOrg']," "));
				$userinfo['profile_field_division'] = strtolower(trim($result['demoOrg2']," "));
				$userinfo['profile_field_unit'] = strtolower(trim($result['demoOrg3']," "));
				$userinfo['profile_field_suburb'] = strtolower(trim($result['demoSuburb']," "));
				$userinfo['profile_field_postcode'] = trim($result['demoPcode']," ");			
			}	
				$userinfo['profile_field_gender'] = strtolower(trim($nusoapresult['gender']," "));
				$userinfo['profile_field_membercode'] = strtolower(trim($nusoapresult['memcode']['Query']['reg_code']," ")); 

			foreach($userinfo as $key => $value){  
				if(!$value){
					unset($userinfo[$key]); 
				} 
			}
		}
		else{
			$message = 'No userinfo exists';
			throw new Exception($message);
		}
		return $userinfo; 
	}
	

	function update_user($username){
		global $DB, $CFG, $USER;
		require_once($CFG->dirroot."/user/profile/lib.php");

		$newuser = new stdClass();
		$getuser = $DB->get_record('user', array('username'=>$username));
		$ipaauser = $this->nusoap_config($username, $GLOBALS['passwordtemp']);
		$newuser->id = $getuser->id;
		if(isset($ipaauser['userinfo'])){
			foreach($ipaauser['userinfo'] as $info){
				$getuser->idnumber = $info['demoRef_id'];
				$getuser->firstname = strtolower(trim($info['demoGiv']," "));
				$getuser->lastname = strtolower(trim($info['demoSur']," "));
				$getuser->email = strtolower(trim($info['demoE_mail']," "));
				//custom profile fields
				$newuser->profile_field_state = strtolower(trim($info['demoState']," "));
				$newuser->profile_field_dob = trim($info['demoDob']," ");
				$newuser->profile_field_position = strtolower(trim($info['demoPosit']," "));
				$newuser->profile_field_organisation = strtolower(trim($info['demoOrg']," "));
				$newuser->profile_field_division = strtolower(trim($info['demoOrg2'], " "));
				$newuser->profile_field_unit = strtolower(trim($info['demoOrg3']," "));
				$newuser->profile_field_suburb = strtolower(trim($info['demoSuburb']," "));
				$newuser->profile_field_postcode = trim($info['demoPcode']," ");	
				}
				$newuser->profile_field_gender = strtolower(trim($ipaauser['gender']," "));
				$newuser->profile_field_membercode = strtolower(trim($ipaauser['memcode']['Query']['reg_code']," "));
		
			$DB->update_record('user',$getuser);
			profile_save_data($newuser);
		}
		else{
			$message = 'No userinfo updated!';
			throw new Exception($message);
		}

	}
	

	function can_change_password() {
        return false;
    }

    
    function change_password_url() {
        return null;
    }

    
    function can_edit_profile() {
        return true;
    }

    
    function is_internal() {
       return false;
    }
   
  	
  	function is_synchronised_with_external() {
       return true;
    }


}